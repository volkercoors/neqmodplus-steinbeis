Note:

All files herein require     **Sketchup 8 or above**
    

*  CityGML surface types are classified in different layers and are visible and usable in native Sketchup.
*  Further CityGML specific features ( like Location Info, Surface Types, Grouping and Attributes ) are stored in the models but are only visible, editable and usable with **CityEditor plugin from 3DIS** ( see [https://www.3dis.de/cityeditor] )

*Note the licensing conditions in the LICENSE file ( also referenced to in the model info of each Sketchup file )*